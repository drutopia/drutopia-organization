This is a template that can be used when proposing Drutopia as a platform to a potential client.


[Insert Company Name]

A proposal for [Insert Client Name]

[Proposal Name - Example: CircleBoston.org Upgrade]

Prepared by:
[Full Name]
[Company Name]
[Company Address]
[Company Phone]

Prepared for:
[Full Name]
[Job Title]
[Organization]

# Project Overview

The [Client Name] seeks a vendor partner to help create a new website built with an open source content management system (CMS). Using our collaborative process, [Company Name] will leverage the open source power of Drupal and the platform cooperative model of Drutopia to build a website that is relevant, feature-rich and regularly improved beyond launch to help [Client Name] meet its mission.

Drutopia is a suite of ready made features built in Drupal which we can leverage to reduce cost and focus our energy towards any custom design or development that will benefit [Client Name].

From reviewing your goals and audiences we recommend the following features, with the understanding our agile method allows for flexibility throughout the project:

* Configurable homepage - that include but is not limited to any of the following: prominent elevator pitch, recent news feed, featured resources, upcoming events, partner organizations list, and a list of current campaigns.
* Actions
* Campaigns
* Events
* Fundraising
* Groups
* News and/or Blog
* Profile pages
* Social Media Integration

# Content Strategy and Information Architecture Approach

We will work closely with your team to define an effective content strategy for your website and a clean, scalable information architecture. These will result in the following deliverables:
* Website Goals
* User Personas
* Content Style Guide
* Sitemap
* Technical Architecture

# Design Approach

Building off of the content strategy and information architecture work, we will work with your team to provide a compelling a theme that speaks to your audience and reinforces your brand. We begin with our clean, responsive Drutopia theme, with the following features baked in:
* Clean - well organized SCSS adhering to Drupal's theming standards
* Secure - covered by the Drupal Security Team
* Responsive - looks beautiful on any device
* Style without code - a clean user interface is available for customizing the color, layout and fonts.

We then identify and implement the customizations that will set your website apart and best tell your story.

# Development Approach

By leveraging Drutopia, we are able to quickly prototype with your team. After defining the information architecture we turn on all relevant features, and stub out the pages in the sitemap. We put on a collaborative training on how to work with the site and then allow your team to add content, test out the features and make note of any adjustments that would be helpful to the project.

We will then work closely together to prioritize remaining enhancements to be made to the website. Enhancements that do not make it into the initial build are added to our backlog for the Drutopia project as a whole.

## Continuous Improvements & Support

When joining Drutopia your site launch is not the end, but rather the beginning. We are in constant communication with all of our members about what enhancements we should work on next. Our roadmap is public to all so you know where the project is heading. As a member you can request new features and vote up existing ideas.

## Content Migration

[Company Name] staff will work with the [Client Name] team to create a plan for populating the new website with content.

This plan will consist of manual migration work to move content from existing pages into the new website. Additionally, optional programmatic migration processes are available for moving user data and other content into Drupal. A redirect strategy will also be put in place for pages that are being culled from the old site.

## Accessibility

It is critical that your site be accessible to as many people as possible, including those using screen readers. To that end, all of our work is built to support compliance with [W3C Web Content Accessibility Guidelines (WCAG) 2.0](https://www.w3.org/TR/WCAG20/). The WCAG Guidelines, however, are not comprehensive and so we go beyond those guidelines to ensure high accessibility. Lastly, much of what makes a site accessible happens on the content entry and management side. We will provide resources for your content team so that after the site launches, you can rest assured that what you are creating is reaching as many people as possible.

## SEO

Our developers will also follow SEO best-practice development and utilize Drupal’s range of SEO-related features that allow administrators to edit page titles, implement human-readable and editable URLs, enter MetaTag information and more. For further SEO-related services, we can also recommend SEO consultants.


# Drutopia Membership

Launching with a Drutopia-built site means all of the features and functionality Drutopia has to offer, but also means joining a platform cooperative of like-minded people. The cooperative consists of a one member one vote model and a leadership team elected from its members once a year. At the support rate [Client Name] receives the following:
* Secure hosting with timely security updates
* The ability to request new features and vote up existing ideas
* Quarterly updates on the project
* 24 hour turn around time on support questions
* Ability to coordinate with fellow members to crowdfund special development sprints

# Deliverables and Budget

The following tables outline the project deliverables and their associated estimated costs. All estimates are based on our hourly rate of $150/hour.

Deliverable                Estimated Effort (hours)
Content Strategy documents           20
Information Architecture             40
Design                               40
Development                          100
Migration                            1000
Total Estimated Hours
Total Cost


Drutopia Membership, Hosting & Services $100/month

There will be no license fees for any software.

# Schedule

Milestone                     Date
Project Kickoff
Content Strategy
Information Architecture
Design
Development
Migration
Quality Assurance
Soft Launch
Public Launch
