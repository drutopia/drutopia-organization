There's no code for the Drutopia Organization project.  See the [wiki](https://gitlab.com/drutopia/drutopia-organization/wikis/home).

The structure of the organization is being decided— along with many other questions.  [Put in your input!](https://gitlab.com/drutopia/drutopia-organization/issues)